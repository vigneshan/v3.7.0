# Android agent certificate installation

If an app or network that you want to use needs a certificate it is required that you install the certificate before proceeding
This section describes how to install security certificates on enrolled Android devices via the Entgra IoTS.

## Creating a new policy

1. Go to the **Policy Management** view from the Top Menu

    ![image](1.png)

2. In the next page select **Add New Policy**
3. Creating a new policy comprises of several steps. The first of which is to select the platform. In the next page select **Android** as the platform.

    ![image](2.png)

## Certificate installation

1. The next step involves attaching the certificate. In the left option list select **Certificate Install**. It would be unticked by default. Tick it so that additional options will be listed below it.

    ![image](9.png)

2. Finally, once all settings are satisfactory, press **Save & Publish** to execute the policy onto the enrolled devices.


    ![image](6.png)

## Agent installation

1. Once the certificate has been installed, a notification will popup on the agent informing of the new certificate.

    ![image](10.jpg)

2. Click on this certificate to proceed with the installation.

    ![image](11.jpg)

3. Once installation is successful, you can view the installed certificates through the relevant menu.

    ![image](12.jpg)

{{< hint info >}}
    The certificate installation notification only appears on the agent if enrollment
    is done via BYOD mode. If done on COPE mode, it will install silently
    without user interaction.
{{< /hint >}}