---
bookCollapseSection: true
weight: 9
---
# Working with Windows Devices

The following sections describe the initial configurations that the system administrator needs to carry out in order to be able to work with Windows device types on Entgra IoTS.
