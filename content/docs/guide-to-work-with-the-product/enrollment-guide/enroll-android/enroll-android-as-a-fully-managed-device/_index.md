---
bookCollapseSection: true
weight: 3
---

# Enroll Android as a fully managed device using QR code

{{< hint info >}}
<strong>Pre-requisites</strong>
<ul style="list-style-type:disc;">
    <li>Server is <a href="https://entgra-documentation.gitlab.io/v3.8.0/docs/guide-to-work-with-the-product/download-and-start-the-server-/">downloaded and started</a></li>
    <li>Logged into the server's<a href="https://entgra-documentation.gitlab.io/v3.8.0/docs/guide-to-work-with-the-product/login-to-devicemgt-portal/">device mgt portal</a></li>
    <li>Please follow <a href=" https://entgra-documentation.gitlab.io/v3.8.0/docs/guide-to-work-with-the-product/enrollment-guide/enroll-android/install-agent/ "> install agent section </a></li>
    <li>Optionally, basic <a href="https://entgra-documentation.gitlab.io/v3.8.0/docs/key-concepts/#android ">concepts of Android device management</a> will be beneficial as well. </li>
</ul>
{{< /hint >}}

<iframe width="560" height="315" src="https://www.youtube.com/embed/hux4WJVN4ho" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Open the agent application</li>
    <li>Click continue when disclaimer appears</li>
    <li>In the next screen click "Enroll with QR Code"</li>
    <li>In the server Select device ownership as "COPE"</li>
    <li>Scan QR code that is generated in server</li>
    <li>The enrollment will be hold until user enable device owner</li>
    <li>Enter the following command on terminal or console to enable device owner.
        <br><i>adb shell 
    dpm 
    set-device-owner io.entgra.iot.agent/org.wso2</i> .iot.agent.services.AgentDeviceAdminReceiver
    </li>
    <li>In the device screen click "Agree" to accept the licence</li>
</ul>